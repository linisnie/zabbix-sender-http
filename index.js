const express = require('express');
const index = express();
const ZabbixSender = require('node-zabbix-sender');
const Sender = new ZabbixSender({host: 'localhost'});

index.use(express.urlencoded({ extended: false }));

index.use('/', (req, res) => {
  console.log(req.query);
  Sender.addItem(req.query.host, req.query.key, req.query.value);
  Sender.send(function(err, senderRes) {
    if (err) {
      res.json(err);
      return;
    }
    res.json(senderRes)

  });
});

index.listen(3003, () => {
  console.log('server started');
});
